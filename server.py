# File: server.py

"""Math and logic for the game of Go.

Based on https://en.wikipedia.org/wiki/Rules_of_Go
look there for definitions of Go specific terms.
"""

import collections
import enum


class Point:
	"""Represents a location."""
	def __init__(self, x=0, y=0):
		"""Creates a point (x, y).

		If either x or y is unspecified, it will default to 0.
		"""
		self.x = x
		self.y = y

	def __add__(self, other):
		"""Adds two points as if they were complex numbers.

		Returns self translated with a horizontal component of other.x, and a vertical component of other.y.
		"""
		return Point(self.x + other.x, self.y + other.y)

	def __eq__(self, other):
		"""Returns True if self and other coinside."""
		return self.x == other.x and self.y == other.y

	def __str__(self):
		"""Returns an ordered pair.

		Won't do the alignment properly if x or y are more than 2 chars long (i.e., x or y are have more than 2 digits, or are negitive and have more than 1 digit.
		"""
		return "(%2d, %2d)" % (self.x, self.y)

	def __repr__(self):
		return "(" + str(self.x) + ", " + str(self.y) + ")"

	def toTuple(self):
		return (self.x, self.y)

	def adjacents(self):
		"""Yields each point thats one away from this one in each of the cardinal directions."""
		offsets = (           ( 0,  1),
		            (-1,  0),           ( 1,  0),
		                      ( 0, -1)            )

		for offset in offsets:
			yield self + Point.fromTuple(offset)

	def clone(self):
		"""Returns an exact copy of self.

		The following will always be True:
			self == self.clone()
			self is not self.clone()
		"""
		return Point(self.x, self.y)

	@staticmethod
	def fromTuple(tuple):
		"""Turns a 2 element tuple into a Point"""
		return Point(tuple[0], tuple[1])

	@staticmethod
	def fromArguments(x, y):
		if y is None:
			return x.clone()
		else:
			return Point(x, y)

# Rule 1
class Stone(enum.Enum):
	""" Represents the players' pawns.

	Use None to represent no stone."""
	BLACK = 0
	WHITE = 1

	def __str__(self):
		if self == Stone.BLACK:
			return "BLACK"
		if self == Stone.WHITE:
			return "WHITE"

	def opposite(self):
		if self == Stone.BLACK:
			return Stone.WHITE
		if self == Stone.WHITE:
			return Stone.BLACK
# Rule 2
class Board:
	"""Represents a Go board."""
	def __init__(self, width=19, height=None):
		"""Constructs a new widthxheight board that has no stones on it.

		If neither width nor height is specified, then it will create a 19x19 board. If only width is specified, then height will default to width.
		"""
		if height is None:
			height = width

		# Rule 5
		self.playfield = [[None for y in range(height)] for x in range(width)]

	def __eq__(self, other):
		"""Returns True if the boards are visually identical.

		Returns True if and only if other is a Board with exactly the same number of stones in exactly the same places with exactly the same colors as self.
		"""
		if not isinstance(other, Board):
			return False
		if self.getHeight() != other.getHeight():
			return False
		if self.getWidth() != other.getHeight():
			return False

		for point in self.allPoints():
			if self.get(point) != other.get(point):
				return False
		return True


	def __str__(self):
		u"""Humanreadable representation using multiline string.

		Something like this:
			7  ○
			6  ○○
			5  ○  ●
			4     ●●
			3      ●
			2  ○○
			1  ○○
			0
			 012345678
		It's OK if it doesn't do the numbers for boards bigger than
		10x10.
		"""
		ret = ""
		symbols = {None: ' ', Stone.BLACK: '○', Stone.WHITE: '●'}

		for y in reversed(range(self.getHeight())):
			if self.getHeight() < 10:
				ret += str(y)
			for x in range(self.getWidth()):
				ret += symbols[self.get(x, y)]
			ret += '\n'
		if self.getHeight() < 10:
			ret += ' '
			for x in range(self.getWidth()):
				ret += str(x)
		return ret

	def getWidth(self):
		"""Returns width of the board."""
		return len(self.playfield)

	def getHeight(self):
		"""Returns height of the board."""
		return len(self.playfield[0])

	def get(self, x, y=None):
		"""Returns the color of the stone at point.

		x can either be a point, or x and y can be ints.
		"""
		point = Point.fromArguments(x, y)
		return self.playfield[point.x][point.y]

	def set(self, color, x, y=None):
		"""Sets the stone at point to color.

		x can either be a point, or x and y can be ints.
		"""
		point = Point.fromArguments(x, y)
		self.playfield[point.x][point.y] = color

	def exists(self, x, y=None):
		"""Returns True if point is a valid location on the board."""
		point = Point.fromArguments(x, y)
		return 0 <= point.x < self.getWidth() and 0 <= point.y < self.getHeight()

	def allPoints(self, maxX=None, maxY=None):
		"""Yields every point that exists."""

		if maxX is None:
			maxX = self.getWidth()
		if maxY is None:
			maxY = self.getHeight()

		for x in range(maxX):
			for y in range(maxY):
				yield Point(x, y)

	def allEmpties(self, maxX=None, maxY=None):
		"""Yields all points that don't have a stone on them"""
		for point in self.allPoints(maxX, maxY):
			if self.get(point) is None:
				yield point

	def allGroups(self, color):
		"""Yields every group of the color."""
		ret = []
		for point in self.allPoints():
			if self.get(point) == color and not Board.groupTupleHasPoint(ret, point):
				ret.append(self.findGroup(point))
		return tuple(ret)

	@staticmethod
	def groupTupleHasPoint(groups, point):
		for group in groups:
			if point in group:
				return True
		return False

	def adjacents(self, x, y=None):
		"""Yields all points adjacents to point that exist."""
		point = Point.fromArguments(x, y)
		for adjacent in point.adjacents():
			if self.exists(adjacent):
				yield adjacent

	def findGroup(self, x, y=None):
		"""Return a tuple of all points that are connected to point.

		Specifically, a point is considered connected to annother if it is the same color and adjacent to that point, or it is connected to a point that also is connected to it.
		"""
		ret = [Point.fromArguments(x, y)]
		queue = [ret[0]]
		color = self.get(ret[0])

		while len(queue) > 0:
			point = queue.pop()
			for adjacent in self.adjacents(point):
				if adjacent not in ret and self.get(adjacent) == color:
					ret.append(adjacent)
					queue.append(adjacent)

		return tuple(ret)


	def findLiberties(self, x, y=None):
		"""Yeilds all of the point's Liberties.

		If the stone is None, then nothing will be yeilded. If y is None, then x can be either a point or a tuple of every point in a group.
		"""
		if not isinstance(x, collections.Iterable):
			point = Point.fromArguments(x, y)
			x = self.findGroup(point)

		for groupMember in x:
			for adjacent in self.adjacents(groupMember):
				if self.get(adjacent) is None:
					yield adjacent

	def capture(self, color):
		"""Remove all libertyless groups"""
		for group in self.allGroups(color):
			liberties = tuple(self.findLiberties(group))
			if len(liberties) is 0:
				for point in group:
					self.set(None, point)

	def clone(self):
		"""Returns an exact copy of self.

		The following will always be True:
			self == self.clone()
			self is not self.clone()
		"""
		ret = Board(self.getWidth(), self.getHeight())
		for point in self.allPoints():
			ret.set(self.get(point), point)
		return ret


class GameLogic:
	"""The Game server.

	Handles all of the rules.
	"""

	def __init__(self, width=19, height=None):
		"""Starts a new game on a widthxheight board."""
		self.previousBoards = []
		self.currentBoard = Board(width, height)
		self.currentPlayer = Stone.BLACK # Rule 6
		self.passCount = 0

	def __str__(self):
		ret = str(self.currentBoard) + "\n"
		if not self.isGameOver():
			ret += " Current Player: " + str(self.currentPlayer)
		else:
			ret += " GAME OVER"
		return ret

	# Rule 7
	def turn(self, x, y=None):
		"""Attempts to do a turn for the current player.

		Returns weather or not the turn was successful. Pass in None for x to cause the current player to pass.
		"""
		if not self.isGameOver():
			if x is None: # Pass
				self.passCount += 1
				self.changePlayer()
				return True
			else: # Play
				# Step 1. (Playing a stone)
				if self.currentBoard.get(x, y) is not None:
					return False
				else:
					newBoard = self.currentBoard.clone()
					newBoard.set(self.currentPlayer, x, y)
					# Step 2. (Capture)
					newBoard.capture(self.currentPlayer.opposite())
					# Step 3. (Self-capture)
					newBoard.capture(self.currentPlayer)

					# Rule 8
					if newBoard in self.previousBoards:
						return False
					else:
						self.previousBoards.append(self.currentBoard)
						self.currentBoard = newBoard
						self.changePlayer()
						self.passCount = 0
						return True


	def changePlayer(self):
		self.currentPlayer = self.currentPlayer.opposite()

	# Rule 9
	def isGameOver(self):
		return self.passCount >= 2

	# Rule 10
	def getWinner(self):
		blackSum = 0
		whiteSum = 0

		for point in self.currentBoard.allPoints():
			if self.currentBoard.get(point) == Stone.BLACK:
				blackSum += 1
			elif self.currentBoard.get(point) == Stone.WHITE:
				whiteSum += 1

		if blackSum > whiteSum:
			return Stone.BLACK
		elif whiteSum > blackSum:
			return Stone.WHITE
		else:
			return "Tie"
