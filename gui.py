# File: gui.py

import pyglet
from pyglet.gl import *
from pyglet.window import mouse

from server import *

go = GameLogic(9)

tile = pyglet.resource.image('assets/tile.png')
blackImage = pyglet.resource.image('assets/black.png')
whiteImage = pyglet.resource.image('assets/white.png')

gameOver = pyglet.text.Label()

stoneTextures = [blackImage, whiteImage]
for texture in stoneTextures:
	texture.anchor_x=int(texture.width/2)
	texture.anchor_y=int(texture.height/2)

black = pyglet.sprite.Sprite(blackImage)
white = pyglet.sprite.Sprite(whiteImage)
stoneSprites = {Stone.BLACK: black, Stone.WHITE: white}

# Used for when the player is hovering over a tile
gostStoneSprites = dict()
for key in stoneSprites:
	original = stoneSprites[key]
	gost = pyglet.sprite.Sprite(original.image)
	gost.opacity = 128
	gost.visible = False
	gostStoneSprites[key] = gost

margin = tile.width
# NOTE: The last row and column do not need their own square, they just use the edge of the previous one
screenWidth = (go.currentBoard.getWidth()-1) * tile.width + 2 * margin
screenHeight = (go.currentBoard.getHeight()-1) * tile.height + 2 * margin

window = pyglet.window.Window(width=screenWidth, height=screenHeight, caption="Go")
window.set_icon(blackImage)
glClearColor(220/255, 179/255, 92/255, 1)
glEnable(GL_BLEND)
glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)

def pointToPixel(x, y=None):
	"""Converts a point representing a location on the board to a point representing a location on the screen."""
	ret = Point.fromArguments(x, y)
	ret.x = ret.x * tile.width + margin
	ret.y = ret.y * tile.height + margin
	return ret

def getCurrentGost():
	return gostStoneSprites[go.currentPlayer]

@window.event
def on_draw():
	window.clear()

	# Draw the board (See the above note)
	for boardPoint in go.currentBoard.allPoints(maxX=go.currentBoard.getWidth()-1, maxY=go.currentBoard.getHeight()-1):
		drawPoint = pointToPixel(boardPoint)
		tile.blit(drawPoint.x, drawPoint.y)

	# Draw the Stones
	for boardPoint in go.currentBoard.allPoints():
		try:
			drawPoint = pointToPixel(boardPoint)
			toDraw = stoneSprites[go.currentBoard.get(boardPoint)]
			toDraw.position = drawPoint.toTuple()
			toDraw.draw()
		except KeyError:
			pass

	# Draw where the player will put the next stone, if they click
	getCurrentGost().draw()

	if go.isGameOver():
		if go.getWinner() == Stone.BLACK:
			gameOver.text = "Black wins!"
			gameOver.color = (0, 0, 0, 255) # White
		elif go.getWinner() == Stone.WHITE:
			gameOver.text = "White wins!"
			gameOver.color = (255, 255, 255, 255) # Black
		else:
			gameOver.text = "Draw!"
			gameOver.color = (128, 128, 128, 255) # Grey
		gameOver.draw()

# The point that the user is currently hovering over
currentPoint = None

@window.event
def on_mouse_motion(x, y, dx, dy):
	global currentPoint

	if not go.isGameOver():
		# Test to see if the mouse is in the radius of any potential stone spaces
		for point in go.currentBoard.allPoints():
			center = pointToPixel(point)
			# Radius arround the given poit.
			# Multiplying it by 0.75 isn't necessary, but gives the interface a better 'feel'
			r = 0.75 * gostStoneSprites[go.currentPlayer].width
			if (x - center.x)**2 + (y - center.y)**2 < r**2:
				currentPoint = point
				getCurrentGost().position = center.toTuple()
				if go.currentBoard.get(currentPoint) is None:
					getCurrentGost().visible = True
				else:
					getCurrentGost().visible = False
				return

	getCurrentGost().visible = False
	currentPoint = None

@window.event
def on_mouse_press(x, y, button, modifiers):
	if button == mouse.RIGHT:
		go.turn(None)
	elif currentPoint is not None and button == mouse.LEFT:
		go.turn(currentPoint)

	on_mouse_motion(x, y, 0, 0)

pyglet.app.run()
