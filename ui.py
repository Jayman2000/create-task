import server

game = server.GameLogic(3)

def getCoordinate(message, max):
	while True:
		response = input(message)
		if response == "p" or response == "pass":
			return None
		else:
			try:
				response = int(response)
				if response not in range(max+1):
					print("Must be between 0 and " + str(max))
				else:
					return response
			except ValueError:
				print("Please enter a whole number.")

try:
	while not game.isGameOver():
		print(game)
		x = getCoordinate("x=", game.currentBoard.getWidth()-1)
		if x is not None:
			y = getCoordinate("y=", game.currentBoard.getHeight()-1)

		if x is None or y is None:
			game.turn(None) # Pass
		else:
			game.turn(x,y)
except KeyboardInterrupt:
	print("\nQuiting Game.")
